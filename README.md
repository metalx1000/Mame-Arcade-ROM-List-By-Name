# Mame-Arcade-ROM-list-by-Name

Copyright Kris Occhipinti 2023-11-18

(https://filmsbykris.com)

License GPLv3

# What is this
When it comes to Aracade ROMS it can be hard to know which file goes to which game.

This is a list to help solve that problem

# Basic Useage
```bash
url="https://gitlab.com/metalx1000/Mame-Arcade-ROM-List-By-Name/-/raw/master/rom.lst"
wget -qO- "$url"|fzf --prompt "Enter Game Title: "
```
- Try and Download ROM
```bash
#search game
url="https://gitlab.com/metalx1000/Mame-Arcade-ROM-List-By-Name/-/raw/master/rom.lst"
rom="$(wget -qO- "$url"|fzf --prompt "Enter Game Title: ")"
[[ $rom ]] || exit

#download rom
wget -c "https://archive.org/download/mame-merged/mame-merged/${rom}.zip"
```
# Create Your Own List 
```bash
#!/bin/bash
url="https://raw.githubusercontent.com/libretro/fbalpha2012/master/svn-current/trunk/gamelist.txt"
list="$(wget -qO- "$url"|grep '|'|cut -d\| -f2,4|sed 's/^ //g'|sed 's/| /|/g'|tr -d "\t")"
echo $list|while read game
do
   file=${game%%|*}
   title=${game##*|}
   echo "$title|$file"
done > rom.lst
```
