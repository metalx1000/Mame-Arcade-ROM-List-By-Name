#!/bin/bash
###################################################################### 
#Copyright (C) 2023  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation version 3 of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

rm tmp.lst 2>/dev/null

echo "Creating Game List..."
list="$(cat all.xml|grep "software name=" -A1|tr -d "\n")"
list="$(echo "$list"|sed 's/<software name="/\n/g'|tr -d "\t"|sed 's/"><description>/|/g'|cut -d\< -f1)"
echo "$list"|while read game
do
   file=${game%%|*}
   title=${game##*|}
   echo "$title|$file"
done|cut -d\" -f1 >> tmp.lst 

echo "Retrieving ROM List..."
#create list of avalable roms
wget -qO- "https://archive.org/download/mame-merged/mame-merged/"|grep '.zip'|cut -d\" -f2 > archive.lst


echo "Cleaning Up List"
cat archive.lst|cut -d\. -f1|while read line;do grep "|$line$" tmp.lst;done|sort -u > rom.lst 

wget -qO- "https://gitlab.com/metalx1000/Mame-Arcade-ROM-List-By-Name/-/raw/master/fixed.lst" >> rom.lst

rm tmp.lst
cd www || exit 1
./create_html.sh
